<?php
/**
 * @file
 * Hooks provided by the wiacapi module.
 */

/**
 * Declare the parameters of this implementation.
 *
 * @return
 *   An array of parameters for this implementation.
 *
 *   Parameters:
 *    - 'autocomplete_path'
 *         Drupal menu path for autocomplete function.
 *    - 'trigger_char'
 *         Character that will start to trigger the autocomplete pattern.
 *    - 'trigger_count'
 *         Count when autocomplete will start.
 *    - 'tag_format'
 *         Format that the tag will be inserted into html as.
 *         Wiacapi will replace the words 'tag' with the matched key that
 *         is returned in autocomplete function.
 *         (Ex. tag_format = {#tag}, matched key = sam, inserted html = {#sam}).
 */
function hook_wiacapi_implementations() {
  return array(
    'myimplementation1' => array(
      'autocomplete_path' => 'ajax/myimplementation1/search',
      'trigger_char' => '@',
      'trigger_count' => 2,
      'tag_format' => '[@tag]',
    ),
  );
}
