/**
 * @file 
 * Plugin to allow ckeditor inline autocomplete functionality
 */
( function($){
  
  CKEDITOR.plugins.add( 'inline_autocomplete',
  {
    init : function( editor )
    {
      var charCount = 0;
      var triggered = false;
      var contents = '';
		  var triggerPos = 0;
      var startElement = '';
      var search_contents = '';
      var selection = '';

      var selectId = 'wia-select-' + editor.name;
      var selectElement;
      var ckeditorEventThemeSpace = 'uiSpace';
      var getSelectElement = function()
      {
        if ( !selectElement )
          selectElement = CKEDITOR.document.getById( selectId );
        return selectElement;
      };

      if (Drupal.ckeditor_ver == 3) {
        ckeditorEventThemeSpace = 'themeSpace';
      }

      editor.on( ckeditorEventThemeSpace, function( event )
      {
        if ( event.data.space == 'top' )
        {
          event.data.html +=
          '<div id="'+selectId+'" class="wia-select hidden"></div>';
        }
      });
      

      editor.on('key', function(e){
        for (var key in Drupal.settings.wiacapi) {
          var obj = Drupal.settings.wiacapi[key];
          var callback = obj.autocomplete_path;
          var triggerChar = obj.trigger_char;
          var triggerCount = obj.trigger_count;
          var tagFormat = obj.tag_format;
          
          var check_selection = editor.getSelection();
          var check_contents = check_selection.getStartElement().getText();
          var check_pos = check_selection.getRanges()[ 0 ].startOffset;
          var check_char = check_contents.substring(check_pos-1, check_pos);
          
          if (check_char == triggerChar) {
            console.log('trigger');
            charCount = 0;
            triggered = true;
            selection = editor.getSelection();
            startElement =  selection.getStartElement();
            triggerPos = selection.getRanges()[ 0 ].startOffset-1;
            console.log('tpos'+triggerPos);
          }
          if (charCount >= triggerCount) { 
            console.log('cc'+charCount);
            contents = startElement.getText();
            console.log('startelement:'+contents);
            var currentPos = editor.getSelection().getRanges()[ 0 ].startOffset;
            console.log('pos'+currentPos);
            search_contents = contents.substring(triggerPos, currentPos);
            console.log(search_contents);
            if (search_contents.substr(0,1) == triggerChar) {
              show_suggestions(search_contents.substr(1), callback, key);
            }
          }
          if (triggered) {
            charCount++;
          }
        }
      });
      
      editor.on('contentDom', function() {
        var selectElement = getSelectElement();
        selectElement.on('click', function(e){
          var impKey = selectElement.data('impKey');
          data = e.data.getTarget().getId();
          tagOuter = Drupal.settings.wiacapi[impKey].tag_format.split('tag');
          new_contents = contents.replace(search_contents, (tagOuter[0] + data + tagOuter[1]));
          selectElement.hide();
          startElement.setText(new_contents);
        })
      });

      function show_suggestions(string, callback, imp_key) {
        console.log(string);
        var selectElement = getSelectElement();
        selectElement.data( 'impKey', imp_key );
        // Send ajax callback
        var request = $.ajax({
          type: 'GET',
          url: '/' + callback + '/' + string,
          dataType: 'json',
          success: function (data) {
            if (data != null) {
              // Success.
              var output = '<ul>';
              for (var key in data) {
                output += '<li id="' + key + '">' + data[key] + '</li>'
              }
              output += '</ul>';

              coords = getCoords();
              selectElement.setStyle('top', coords['y']+'px');
              selectElement.setStyle('left', coords['x']+'px');              
              selectElement.setHtml(output);              
              selectElement.show();
              selectElement.removeClass('hidden');
            }
            else {
              // Failure.
              console.log(data.errorMessage);

            }
          }
        });
      }

      function getCoords() {
        var dummyElement = editor.document.createElement( 'img',
        {
           attributes :
           {
              src : 'null',
              width : 0,
              height : 0
           }
        });

        editor.insertElement( dummyElement );

        var x = 0;
        var y  = 15;

        var obj = dummyElement.$;

       // Get offSetPos from IFrame-->Up
       var textareaWrapper = CKEDITOR.document.getById( editor.name ).getParent();
       var el  = editor.container.getChild(1).getChild(1).$;
       while (el){
           x += el.offsetLeft;
           y += el.offsetTop;
           el = el.offsetParent;
        }

        // Get offSetPos from IFrame-->Down[/b]
        while (obj.offsetParent){
           x += obj.offsetLeft;
           y  += obj.offsetTop;
           obj    = obj.offsetParent;
        }

        // Account for left/right scrolling
        var scrollTop = editor.document.$.documentElement.scrollTop;
        var scrollLeft = editor.document.$.documentElement.scrollLeft;

        x += obj.offsetLeft-scrollLeft;
        y += obj.offsetTop-scrollTop ;

        //window.parent.document.title = "top: " + y + ", left: " + x +  xx: " +scrollLeft+  ", yy: " + scrollTop;
        var coords = new Array();
        coords['x'] = x;
        coords['y'] = y;

        dummyElement.remove();

        return coords;
      }      
    }
  });
})(jQuery);