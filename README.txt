WYSYWG Inline Autocomplete API
Defines a plugin API for the CKEditor module that allows other modules to define
inline autocomplete lookups for users as they are entering content.
https://drupal.org/sandbox/shawn_smiley/2168131



REQUIREMENTS
The CKEditor module or the Wysiwyg module
The CKEditor editor



INSTALLATION
Copy the wysywg_inline_autocomplete_api to your sites/all/modules directory.
Go to admin/modules and enable the module.

*Set permissions*
Go to admin/people/permissions and grant the WIACAPI related permissions
to the desired roles.

*When using the CKEditor module*
Go to admin/config/content/ckeditor and edit the desired profile.
Under "Editor appearance" > "Plugins", check the "Inline Autocomplete" box.
Save changes.

*When using the Wysiwyg module*
Go to admin/config/content/wysiwyg and edit the desired CKEditor-enabled input
format.
Under "Buttons and plugins", check "Inline Autocomplte" box.
Save changes.

*Turn on implementations of the WIACAPI API
Download and install module that implements hook_wiacapi_implementations().
Or Develop module to implement hook_wiacapi_implementations().
See wiacapi.api.php
